"use strict";

let newUser = {};

let createNewUser = () => {
  newUser = {
    _firstName: prompt("Введіть ім'я:"),
    _lastName: prompt("Введіть прізвище:"),

    get firstName() {
      return this._firstName;
    },

    get firstName() {
      return this._lastName;
    },

    setFirstName: function (newFirstName) {
      this._firstName = newFirstName;
    },
    setLastName: function (newLastName) {
      this._lastName = newLastName;
    },
    getLogin: function () {
      return `${this._firstName[0]}${this._lastName}`.toLowerCase();
    },
  };
  return newUser;
};

Object.defineProperty(newUser, "_firstName", {
  writable: false,
});
Object.defineProperty(newUser, "_lastName", {
  writable: false,
});

createNewUser();

console.log(newUser.getLogin());
